﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, Router } from 'react-router-dom';
import { createHashHistory } from 'history';
import SessionController from './Controllers/SessionController';
import Header from './Header';
import Home from './App/Home';
import Companies from './App/Companies';
import Login from './App/Login';
//import Alerts from './App/Alerts';
import NotFound from './NotFound';
import MobileDetect from 'mobile-detect';
import HomeController from './Controllers/HomeController';

class AppMain extends Component {
    constructor(props) {
        super(props);
        this.history = createHashHistory();
    }

    render() {
        return (
            <Router history={this.history}>
                <Switch>
                    <Route exact path='/' render={props => (
                        <Header>
                            <Home {...props} />
                        </Header>
                    )} />

                    <Route component={NotFound} />
                </Switch>
            </Router>
        )
    }
}

AppMain.displayName = "AppMain";

AppMain.propTypes = {
    children: PropTypes.node
};

export default AppMain;