import React, { Component } from 'react';
import HomeController from './HomeController';
import $ from 'jquery';
const signalR = require("@aspnet/signalr");

let that;
class SessionController extends Component {
    constructor(props) {
        super(props);
        this.IsConnected = false;
        this.HubConnection = null;
        this.HubIsConnected = false;
        that = this;
    }

    CreateHubConnection(promise){
        if(that.HubConnection === null){
            that.HubConnection = new signalR.HubConnectionBuilder().withUrl(HomeController.UrlApi + 'hub').build();
        }

        if(typeof promise === 'function'){
            promise();
        }
    }

    InitHubConnection(){
        if(that.HubConnection !== null && !that.HubIsConnected){
            that.HubIsConnected = true;
            that.HubConnection.start().then(() => {
                that.HubConnection.invoke("Init");
            });
        }
    }

    HandleConnectionComputer(promise){
        that.CreateHubConnection(() => {
            if(typeof promise === 'function'){
                that.HubConnection.on("synchronizeComputer", (computer) => {
                    promise(computer);
                });
                that.InitHubConnection();
            }
        });
    }

    HandleConnectionComputerPerformance(promise){
        that.CreateHubConnection(() => {
            if(typeof promise === 'function'){
                that.HubConnection.on("synchronizeComputerPerformance", (computerPerformance) => {
                    promise(computerPerformance);
                });
                that.InitHubConnection();
            }
        });
    }

    Login(authKey, promise) {
        $.ajax({
            method: "POST",
            url: HomeController.UrlApi + "Users/Login",
            data: {
                AuthKey: authKey,
            },
            success: function (success) {
                if(typeof promise === 'function')
                    promise(success);
            },
        });
    }

    Logout(promise){
        localStorage.clear();
        this.IsConnected = false;
        if(typeof promise === 'function')
            promise();
    }
}


SessionController.displayName = 'SessionController';

export default (new SessionController);