import React, { Component } from 'react';

class HomeController extends Component {
    constructor(props) {
        super(props);
        this.UrlApi = "http://92.222.155.146:5000/";
        this.IsMobile = false;
    }
}

HomeController.displayName = 'HomeController';

export default (new HomeController);