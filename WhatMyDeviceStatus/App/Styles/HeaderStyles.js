﻿const drawerWidth = 240;

const HeaderStyles = theme => ({
    flex:{
        flex: 1
    },
    children:{
        marginTop: 80
    },
    previousbutton: {
        width:40, 
        height: 40,
        marginLeft: -12,
        marginRight: 5,
        boxShadow: 'none',
        background: 'transparent !important',
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: '0 8px',
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
});

export default HeaderStyles;