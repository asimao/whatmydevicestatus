import { textAlignCenter } from './_MainStyles';

const CompaniesStyles = theme => ({
    paperCompanies: {
        maxWidth:296,
        width: '90%',
        margin: '70px auto 10px auto',
        padding: 15,
        overflowX: 'auto',
        flexGrow: 1,
        ...textAlignCenter,
    },
    
    buttonSubmit: {
        marginTop: 15,
        minHeight: '46px !important'
    },

    selectCompanies : {
        marginLeft:15,
        
    }

});

export default CompaniesStyles;