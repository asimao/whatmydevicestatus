import { hidden } from './_MainStyles';
import common from '@material-ui/core/colors/common';

const MainLoaderStyles = theme => ({
    loaderbackground: {
        position: 'fixed !important',
        backgroundColor: common.white,
        zIndex: '2000!important',
        left: '0!important',
        top: '0!important',
        right: '0!important',
        bottom: '0!important',
    },
    mloader: {
        color: "#aaaaaa",
        position: 'fixed !important',
        zIndex: '2020 !important',
        left: 'calc(50% - 40px) !important',
        top: 'calc(50% - 40px) !important',
        width: '80px !important',
        height: '80px !important',
    },
    messageLoaderContent: {
        color: "#bbbbbb !important",
        position: 'fixed',
        zIndex: '2020',
        left: 0,
        fontSize: '20px',
        textAlign: 'center',
        top: 'calc(50% + 80px)',
        textTransform: 'uppercase',
        width: '100%'
    },
    messageLoader: {
        color: "#bbbbbb",
    },
    hidden: {
        ...hidden,
    }
});

export default MainLoaderStyles;