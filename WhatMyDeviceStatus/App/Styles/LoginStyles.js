﻿import { textAlignCenter } from './_MainStyles';

const LoginStyles = theme => ({
    paperLogin: {
        maxWidth:296,
        width: '90%',
        margin: '70px auto 10px auto',
        padding: 15,
        overflowX: 'auto',
        flexGrow: 1,
        ...textAlignCenter,
    },
    textFieldLogin:{
        marginTop: 10,
        marginBottom: 20,
    },
    buttonLogin: {
        marginTop: 15,
        minHeight: '46px !important'
    }
});

export default LoginStyles;