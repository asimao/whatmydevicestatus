import { textAlignCenter } from './_MainStyles';

const AutocompleteStyles = theme => ({
    root: {
        flexGrow: 1,
    },
    container: {
        position: 'relative',
    },
    suggestionsContainerOpen: {
        maxHeight: 185,
        overflow: 'auto',
        position: 'fixed',
        width:256,
        zIndex: 50,
        margin: '-12px auto',
        left: 0,
        right: 0,
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    divider: {
        height: theme.spacing.unit * 2,
    },
    textAlignCenter:{
        ...textAlignCenter
    },
    textfield:{
        margin: 15,
        [theme.breakpoints.only('xs')]: {
            margin: '15px 0',
          },
    },
});

export default AutocompleteStyles;