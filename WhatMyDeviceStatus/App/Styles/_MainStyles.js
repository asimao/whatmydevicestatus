﻿const textAlignLeft = {
    textAlign: 'left'
};
const textAlignCenter = {
    textAlign: 'center'
};
const textAlignRight = {
    textAlign: 'right'
};
const hidden = {
    display: 'none !important'
};
const marginauto = {
    margin: 'auto'
};

export {
    textAlignLeft,
    textAlignCenter,
    textAlignRight,
    hidden,
    marginauto
};