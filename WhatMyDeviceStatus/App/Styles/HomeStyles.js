﻿import { marginauto, textAlignCenter, textAlignRight } from './_MainStyles';
import common from '@material-ui/core/colors/common';
import { red, deepOrange, orange, amber, blue, lightBlue } from '@material-ui/core/colors';
const drawerWidth = 240;

const HomeStyles = theme => ({
  rootGridTitle: {
    flexGrow: 1,
    marginTop: 10,
  },
  rootGrid: {
    flexGrow: 1,
    marginTop: 30,
  },
  mainRootGrid:{
    flexGrow: 1,
    marginTop: 10,
    marginBottom: 30
  },
  marginauto:{
    ...marginauto,
  },
  textAlignCenter:{
    ...textAlignCenter
  },
  buttonNavContent:{
    position: 'fixed',
    zIndex: 1000
  },
  buttonNav:{
    marginRight: 30,
    ...textAlignRight
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  canvas:{
    [theme.breakpoints.only('xs')]: {
      maxWidth: '60%'
    },
    [theme.breakpoints.only('sm')]: {
      maxWidth: '80%'
    },
    maxWidth: '100%'
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  pos: {
    marginBottom: 12,
  },
  paper: {
    padding: 40,
  },

  paperLogs: {
    backgroundColor: lightBlue.A200,
  },

  paperInformations: {
    backgroundColor: blue.A200,
  },

  paperWarning: {
    backgroundColor: amber.A400,
  },

  paperAverage: {
    backgroundColor: orange.A200,
  },

  paperDanger:{
    backgroundColor: deepOrange.A200,
  },

  paperCritical: {
    backgroundColor: red.A200,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
  paperTitle:{
    [theme.breakpoints.only('xs')]: {
      fontSize: 20,
      margin: '10px 15px',
    },
    color: common.white,
    display: 'inline-block',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    fontSize: 26,
    margin: '15px 20px',
  },
  paperIcons:{
    [theme.breakpoints.only('xs')]: {
      width: 35,
      height: 35,
    },
    width: 50,
    height: 50,
    color: common.white,
  },
  paperButtonBase:{
    width: '100%',
  },
  numberTypoPaper:{
    [theme.breakpoints.down('sm')]: {
      fontSize: 20,
      margin: '10px 15px',
    },
    position: 'absolute',
    bottom: '0',
    right: -5,
    color: common.white,
    fontSize: 26,
    margin: '15px 20px',
  }
});

export default HomeStyles;