﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import CircularProgress from '@material-ui/core/CircularProgress';
import MainLoaderStyles from '../Styles/MainLoaderStyles';
import Typography from '@material-ui/core/Typography';

class MainLoader extends Component {
    constructor(props) {
        super(props);
        this.state= {
            Visible: false,
            Message: "",
        }
    }

    componentWillMount(){
        this.setState({
            Visible: this.props.visible,
            Message: this.props.message,
        });
    }

    componentWillReceiveProps(nextProps){
        setTimeout(() => {
            this.setState({
                Visible: nextProps.visible,
                Message: nextProps.message,
            });
        }, !nextProps.visible ? 1000 : 0);
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classNames({[classes.hidden]: !this.state.Visible})}>
                <div className={classes.loaderbackground}></div>
                <CircularProgress size={60} thickness={4} className={classes.mloader} />
                <div className={classes.messageLoaderContent}>
                    <Typography className={classes.messageLoader} dangerouslySetInnerHTML={{__html: this.state.Message}}/>
                </div>
            </div>
        );
    }
}

MainLoader.displayName = 'MainLoader';

MainLoader.propTypes = {
    visible: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
};

export default withStyles(MainLoaderStyles)(MainLoader);