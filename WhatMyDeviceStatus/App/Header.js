﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import HeaderStyles from './Styles/HeaderStyles';
import MainLoader from './Main/MainLoader';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import SessionController from './Controllers/SessionController';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from 'mdi-material-ui/Menu';
import ChevronLeftIcon from 'mdi-material-ui/ChevronLeft';
import Divider from '@material-ui/core/Divider';
import IconHomeOutline from 'mdi-material-ui/HomeOutline';
import IconFileOutline from 'mdi-material-ui/FileOutline';
import IconAccountOutline from 'mdi-material-ui/AccountOutline';
import IconCloseCircleOutline from 'mdi-material-ui/CloseCircleOutline';


class Header extends Component {
    constructor(props) {
        super(props);
        this.state = this.initState();
    }

    initState(){
        return {
            DrawerIsOpen: false,
            MainLoaderIsVisible: true,
            MainLoaderMessage: '',
            AlertsTypes: '',
        };
    }

    componentDidMount(){
        this.setState({
            MainLoaderMessage: "Waiting server's data",
        });
    }

    SetAlertsTypes = (type) => {
        this.setState({
            AlertsTypes: type
        });
    }

    getDrawerList (){
        return [
            {type: "ListItem", label: "Dashboard", icon: <IconHomeOutline/>, function: () => this.handleMoveTo("/home")},
            {type: "ListItem", label: "Entreprises", icon: <IconFileOutline/>, function: () => this.handleMoveTo("/companies")},
            {type: "ListItem", label: "Ordinateurs", icon: <IconAccountOutline/>, function: () => this.handleMoveTo("/computers")},
            {type: "Divider"},
            {type: "ListItem", label: "Déconnexion", icon: <IconCloseCircleOutline/>, function: () => SessionController.Logout(() => this.handleMoveTo("/"))},
        ];
    }

    handleMoveTo = (route) => {
        this.handleCloseDrawer();
        this.context.router.history.push(route);
    }

    handleLoad = (message) => {
        this.setState({
            MainLoaderIsVisible: true,
            MainLoaderMessage: message,
        });
    }

    handleUnLoad = () => {
        this.setState({
            MainLoaderIsVisible: false,
            MainLoaderMessage: '',
        });
    }

    handleOpenDrawer = () => {
        this.setState({
            DrawerIsOpen : true,
        });
    }

    handleCloseDrawer = () => {
        this.setState({
            DrawerIsOpen : false,
        });
    }

    render() {
        const { classes, children } = this.props;
        const childrenWithProps = React.cloneElement(children, { parent: this });

        return (
            <div>
                <MainLoader visible={this.state.MainLoaderIsVisible} message={this.state.MainLoaderMessage}/>
                <AppBar elevation={6} position="fixed">
                    <Toolbar>
                    <IconButton 
                        className={classes.menuButton} 
                        color="inherit"
                        onClick={this.handleOpenDrawer}
                    >
                        <MenuIcon />
                    </IconButton>
                        <Typography
                            color="inherit"
                            variant="h6"
                            className={classes.flex}
                        >
                            WhatMyDeviceStatus
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Drawer 
                    open={this.state.DrawerIsOpen} 
                    onClose={this.handleCloseDrawer}
                    className={classes.drawer}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={this.handleCloseDrawer}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <div className={classes.drawerlist}>
                        <List>
                            {this.getDrawerList().map((e, i) => (
                                e.type == "ListItem" && 
                                <div
                                    key={i}
                                    tabIndex={0}
                                    role="button"
                                    onClick={() => e.function()}
                                    onKeyDown={() => e.function()}
                                >
                                    <ListItem button>
                                        <ListItemIcon>{e.icon}</ListItemIcon>
                                        <ListItemText primary={e.label} />
                                    </ListItem>
                                </div> ||
                                e.type == "Divider" && 
                                <Divider key={i}/>
                            ))}
                        </List>
                    </div>
                </Drawer>
                <div className={classes.children}>{childrenWithProps}</div>
            </div>
        );
    }
}

Header.displayName = "Header";

Header.propTypes = {
    classes: PropTypes.object.isRequired,
    children: PropTypes.node,
};

Header.contextTypes = {
    router: PropTypes.object.isRequired,
};

export default withStyles(HeaderStyles)(Header);