import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SessionController from '../Controllers/SessionController';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import { withStyles } from '@material-ui/core/styles';
import HomeStyles from '../Styles/HomeStyles';
import classNames from 'classnames';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from 'mdi-material-ui/Home';
import DashboardIcon from 'mdi-material-ui/DesktopMacDashboard';
import ApplicationIcon from 'mdi-material-ui/Application';
import ChartIcon from 'mdi-material-ui/ChartBar';


const listDrawer = [
    {label: 'Dashboard', icon: <DashboardIcon />}, 
    {label: 'Logiciels', icon: <ApplicationIcon />}, 
    {label: 'Graphiques', icon: <ChartIcon />}, 
    {label: 'Drafts', icon: <HomeIcon />}
];


class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            anchorEl: null,
            mobileMoreAnchorEl: null,
            DrawerOpen: false,
            SelectedComputer: false, //utiliser plus tard 
        };
    }

    componentDidMount() {
        console.log('hub connection');
        SessionController.HandleConnectionComputer((computer) => {
            console.log('computer received');
            //console.log(computer);
        });

        SessionController.HandleConnectionComputerPerformance((computerPerformance) => {
            console.log('computer performance received');
            //console.log(computerPerformance);
        });
    }

    handleProfileMenuOpen = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleMenuClose = () => {
        this.setState({ anchorEl: null });
        this.handleMobileMenuClose();
    };

    handleMobileMenuOpen = event => {
        this.setState({ mobileMoreAnchorEl: event.currentTarget });
    };

    handleMobileMenuClose = () => {
        this.setState({ mobileMoreAnchorEl: null });
    };

    handleDrawerOpen = () => {
        this.setState({ DrawerOpen: true });
    };

    handleDrawerClose = () => {
        this.setState({ DrawerOpen: false });
    };

    

    render() {   
        const { anchorEl, mobileMoreAnchorEl,DrawerOpen } = this.state;
        const { classes,theme } = this.props;
        const isMenuOpen = Boolean(anchorEl);
        const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

        const renderMenu = (
            <Menu
                anchorEl={anchorEl}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={isMenuOpen}
                onClose={this.handleMenuClose}
            >
                <MenuItem onClick={this.handleMenuClose}>Profile</MenuItem>
                <MenuItem onClick={this.handleMenuClose}>My account</MenuItem>
            </Menu>
        );

        const renderMobileMenu = (
            <Menu
                anchorEl={mobileMoreAnchorEl}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={isMobileMenuOpen}
                onClose={this.handleMenuClose}
            >
                <MenuItem onClick={this.handleMobileMenuClose}>
                    <IconButton color="inherit">
                        <Badge badgeContent={4} color="secondary">
                            <MailIcon />
                        </Badge>
                    </IconButton>
                    <p>Messages</p>
                </MenuItem>
                <MenuItem onClick={this.handleMobileMenuClose}>
                    <IconButton color="inherit">
                        <Badge badgeContent={11} color="secondary">
                            <NotificationsIcon />
                        </Badge>
                    </IconButton>
                    <p>Notifications</p>
                </MenuItem>
                <MenuItem onClick={this.handleProfileMenuOpen}>
                    <IconButton color="inherit">
                        <AccountCircle />
                    </IconButton>
                    <p>Profile</p>
                </MenuItem>
            </Menu>
        );

        return (
            <div className={classes.root}>
                <AppBar
                    position="fixed"
                    className={classNames(classes.appBar, {
                      [classes.appBarShift]: DrawerOpen,
                    })}
                >
                    <Toolbar disableGutters={!DrawerOpen}>
                    <IconButton
                      color="inherit"
                      aria-label="Open drawer"
                      onClick={this.handleDrawerOpen}
                      className={classNames(classes.menuButton, DrawerOpen && classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>

                    <Drawer
                            className={classes.drawer}
                            variant="persistent"
                            anchor="left"
                            open={DrawerOpen}
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                        >
                            <div className={classes.drawerHeader}>
                                <IconButton onClick={this.handleDrawerClose}>
                                    {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                                </IconButton>
                            </div>
                            <Divider />
                            <List>
                                {listDrawer.map((obj, index) => (
                                    <ListItem button key={index}>
                                        <ListItemIcon>{obj.icon}</ListItemIcon> 
                                        <ListItemText primary={obj.label} />
                                    </ListItem>
                                ))}   
                            </List>
                            <Divider />
                            {/* <List>
                                {['All mail', 'Trash', 'Spam'].map((text, index) => (
                                    <ListItem button key={text}>
                                        <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                                        <ListItemText primary={text} />
                                    </ListItem>
                                ))}
                            </List> */}
                        </Drawer>


                        <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                            WhatMyDeviceStatus
                        </Typography>
                        <div className={classes.grow} />
                        <div className={classes.sectionDesktop}>
                            <IconButton
                                aria-owns={isMenuOpen ? 'material-appbar' : undefined}
                                aria-haspopup="true"
                                onClick={this.handleProfileMenuOpen}
                                color="inherit"
                            >
                                <AccountCircle />
                            </IconButton>
                        </div>
                        <div className={classes.sectionMobile}>
                            <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
                                <MoreIcon />
                            </IconButton>
                        </div>
                    </Toolbar>
                    </AppBar>
                {renderMenu}
                {renderMobileMenu}
            </div>
        );


        


    }
}

Home.displayName = 'Home';

Home.contextTypes = {
    router: PropTypes.object.isRequired,
};

Home.propsTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(HomeStyles, { withTheme: true })(Home);