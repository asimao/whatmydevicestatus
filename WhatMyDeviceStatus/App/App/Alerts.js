import React, { Component } from 'react';
import PropTypes, { bool } from 'prop-types';
import SessionController from '../Controllers/SessionController';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import AlertsStyles from '../Styles/AlertsStyles';


import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';
import AlertDecagramOutlineIcon from 'mdi-material-ui/AlertDecagramOutline';
import ExclamationIcon from 'mdi-material-ui/Exclamation';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

class Alerts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
        };
    }
    


    componentDidMount() {
        SessionController.HandleConnectionComputer(() => {
            
        });

        SessionController.HandleConnectionComputerPerformance(() => {
            
        });
    }  

    componentDidUpdate(){
        
    }

    handleClick = () => {
        this.setState(state => ({ open: !state.open }));
    };
    
     
    render() {   
        const { classes,  } = this.props;

        
        const alertsText = [
            {computerName:"PC1",text:"Le pc de micheline a son disque plein",companyName:"Kayac", DateOfAlert:"<30Sec",icon :""},
            {computerName:"PC2",text:"Le pc de micheline A",companyName:"Jm", DateOfAlert:"<30Sec",Gravity:"1",icon :""},
            {computerName:"PC2",text:"Le pc de micheline B",companyName:"razoul", DateOfAlert:"<30Sec",Gravity:"2",icon :""},
            {computerName:"PC2",text:"Le pc de micheline C",companyName:"francis", DateOfAlert:"<30Sec",Gravity:"3",icon :""},
            {computerName:"PC2",text:"Le pc de micheline C",companyName:"Jaques", DateOfAlert:"<30Sec",Gravity:"2",icon :""},
        ];

        alertsText.map((o, i) => {
            if(o.Gravity != null || o.Gravity != ""){
                
                switch (o.Gravity) {
                    case "1":
                        o.icon=<AlertDecagramOutlineIcon/>;
                        break;

                    case "2":
                        o.icon=<AlertDecagramOutlineIcon/>;
                        break;

                    case "3":
                        o.icon=<AlertDecagramOutlineIcon/>;
                        break;
                
                    default:
                        o.icon=<ExclamationIcon/>;
                        break;
                }
            }else{
                o.icon=<ExclamationIcon/>;
            }
        } )
        
        return (
            
            <List className={classes.rootList} subheader={<ListSubheader component="div">Liste des alertes</ListSubheader>} >
                
                {
                    alertsText.map((o, i) => {
                    
                    
                        
                    return(
                        <div>
                            <ListItem button onClick={this.handleClick}>
                                <ListItemIcon>
                                    {o.icon}
                                </ListItemIcon>
                                <ListItemText primary={o.computerName+" "+o.companyName+" "+o.DateOfAlert} secondary={o.text} >
                                </ListItemText> 
                                {this.state.open ? <ExpandLess /> : <ExpandMore />}
                            </ListItem>

                            <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                              <List component="div" disablePadding>
                                <ListItem button className={classes.nested}>
                                  <ListItemText inset primary="ExpandedText Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum." />
                                </ListItem>
                              </List>
                            </Collapse>
                            
                        </div>
                    )
                })
            }
            </List>
            
        );
    }
}

Alerts.displayName = 'Alerts';

Alerts.contextTypes = {
    router: PropTypes.object.isRequired,
};

Alerts.propsTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withWidth()(withStyles(AlertsStyles)(Alerts));