﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import LoginStyles from '../Styles/LoginStyles';
import classNames from 'classnames';
import SessionController from '../Controllers/SessionController';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import { swaldismiss } from '../Tools/SwalTool';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            AuthKey: "",
        };
    }

    handleKeyDownLogin = (e) => {
        if (e.key === 'Enter') {
            this.handleLogin();
        }
    }

    handleLogin = () => {
        SessionController.Login(this.state.AuthKey, (success) => {
            if (!success) {
                this.handleLoginError();
            }
            else {
                SessionController.IsConnected = true;
                this.context.router.history.replace('/companies');
            }
        });
    }

    handleLoginError = () => {
        swaldismiss("Incorrect authentification's key", "warning");
    }

    handleChangeTextField = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                <Paper className={classNames(classes.paperLogin)}>
                    <FormControl>
                        <TextField
                            name="AuthKey"
                            label="AuthKey"
                            value={this.state.AuthKey}
                            onChange={this.handleChangeTextField}
                            variant="outlined"
                            className={classes.textFieldLogin}
                            onKeyDown={this.handleKeyDownLogin}
                        />
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.buttonLogin}
                            onClick={this.handleLogin}
                        >
                            Se connecter
                        </Button>
                    </FormControl>
                </Paper>
            </div>
        );
    }
}

Login.displayName = "Login";

Login.propTypes = {
    classes: PropTypes.object.isRequired,
};

Login.contextTypes = {
    router: PropTypes.object.isRequired,
}

export default withStyles(LoginStyles)(Login);