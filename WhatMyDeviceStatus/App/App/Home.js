import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SessionController from '../Controllers/SessionController';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import HomeStyles from '../Styles/HomeStyles';
import classNames from 'classnames';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import SwipeableViews from 'react-swipeable-views';

import IconButton from '@material-ui/core/IconButton';
import ChevronLeft from 'mdi-material-ui/ChevronLeft';
import ChevronRight from 'mdi-material-ui/ChevronRight';

import Chart from 'chart.js';

import moment from 'moment';
moment.locale('fr');

let that;
let computers = [];
var chartCpu = [];
var chartMemory = [];
var chartDiskDrive = [];

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            View: 0,
            Computers: [],
        };

        that = this;
    }

    componentDidMount() {
        SessionController.HandleConnectionComputerPerformance((cp) => {
            var computer = JSON.parse(cp);

            var index = -1;


            index = computers.findIndex(fi => fi.Id == computer.Id);

            if (computers.findIndex(fi => fi.Id == computer.Id) != -1) {
                computers[index] = computer;
            }
            else {
                computers.push(computer);
            }

            that.setState({
                Computers: computers,
            });


            const option = {
                animation: {
                    duration: 0
                },
                hover: {
                    animationDuration: 0
                },
                responsiveAnimationDuration: 0,
            }

            const optionDisk = {
                animation: {
                    duration: 0
                },
                hover: {
                    animationDuration: 0
                },
                responsiveAnimationDuration: 0,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }

            const numberOfData = 12;

            computers.map((o) => {
                var nData = o.ComputerPerfomance.length;
                var dataComputer = o.ComputerPerfomance.slice((nData-1) - numberOfData, nData);

                var dataCpu = [];
                var dataMemory = [];
                var dataDisk = [];
                var dataDate = [];

                dataComputer.map((o) => {
                    dataCpu.push(o.CpuPerf);
                    dataMemory.push(o.MemoryPerf);
                    dataDisk.push(o.DiskDrivePerf);
                    dataDate.push(moment(o.LastUpdate).format('LTS'));
                })

                if(chartCpu[o.Id])
                    chartCpu[o.Id].destroy();

                if(chartMemory[o.Id])
                    chartMemory[o.Id].destroy();

                if(chartDiskDrive[o.Id])
                    chartDiskDrive[o.Id].destroy();

                chartCpu[o.Id] = new Chart(document.getElementById('CpuPerf' + o.Id).getContext("2d"), {
                    type: 'line',
                    responsive: true,
                    data: {
                        labels: dataDate,
                        datasets: [{
                            label: '% cpu percentage',
                            data: dataCpu,
                            borderWidth: 2,
                            backgroundColor: 'rgba(255, 255, 255, 0)',
                            borderColor: '#D24D3E'
                        }],
                    },
                    options: option
                });

                chartMemory[o.Id] = new Chart(document.getElementById('MemoryPerf' + o.Id).getContext("2d"), {
                    type: 'line',
                    responsive: true,
                    data: {
                        labels: dataDate,
                        datasets: [{
                            label: '% memory percentage',
                            data: dataMemory,
                            borderWidth: 2,
                            backgroundColor: 'rgba(255, 255, 255, 0)',
                            borderColor: '#55A059'
                        }]
                    },
                    options: option
                });

                chartDiskDrive[o.Id] = new Chart(document.getElementById('DiskDrivePerf' + o.Id).getContext("2d"), {
                    type: 'line',
                    responsive: true,
                    data: {
                        labels: dataDate,
                        datasets: [{
                            label: '% disk percentage',
                            data: dataDisk,
                            borderWidth: 2,
                            backgroundColor: 'rgba(255, 255, 255, 0)',
                            borderColor: '#EAB740'
                        }]
                    },
                    options: optionDisk
                });
            })

            that.props.parent.handleUnLoad();
        });
    }

    handleChangeView = (type) => {
        this.setState({
            View: type === 'up' ? this.state.View+1 : this.state.View-1
        });
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                {
                    this.state.Computers.length > 0 &&
                    <Grid container spacing={0} className={classes.buttonNavContent}>
                        <Grid item xs={12} className={classes.buttonNav}>
                            <IconButton 
                                disabled={this.state.View === 0} 
                                onClick={() => this.handleChangeView('down')}
                            >
                                <ChevronLeft />
                            </IconButton>
                            <IconButton 
                                disabled={this.state.View === this.state.Computers.length-1} 
                                onClick={() => this.handleChangeView('up')}
                            >
                                <ChevronRight />
                            </IconButton>
                        </Grid>
                    </Grid>
                }

                <SwipeableViews
                    index={this.state.View}
                    disabled
                >
                    {
                        this.state.Computers.map((o) => (
                            <Grid key={o.Id} container className={classes.mainRootGrid} spacing={0}>
                                <Grid item xs={12} sm={10} lg={8} className={classes.marginauto}>
                                    <Paper elevation={8} className={classes.paper}>
                                        <Grid container className={classes.rootGridTitle} spacing={0}>
                                            <Grid item xs={12} sm={6} className={classNames(classes.marginauto, classes.textAlignCenter)}>
                                                <Typography variant="button" gutterBottom>
                                                    {o.Name}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                        <Grid container className={classes.rootGrid} spacing={0}>
                                            <Grid item xs={12} sm={6} className={classNames(classes.marginauto, classes.textAlignCenter)}>
                                                <Typography variant="button" gutterBottom>
                                                    {"Uuid: " + o.Uuid}
                                                </Typography>
                                                <Typography variant="button" gutterBottom>
                                                    {"Vendor: " + o.Vendor}
                                                </Typography>
                                                <Typography variant="button" gutterBottom>
                                                    {"Add Date: " + moment(o.AddDate).format('DD/MM/YYYY HH:mm:ss')}
                                                </Typography>
                                                <Typography variant="button" gutterBottom>
                                                    {"Last Update: " + moment(o.LastUpdate).format('DD/MM/YYYY HH:mm:ss')}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={12} sm={6} className={classNames(classes.marginauto, classes.textAlignCenter)}>
                                                <Typography variant="button" gutterBottom>
                                                    CPU
                                        </Typography>
                                                <canvas id={"CpuPerf" + o.Id} className={classes.canvas} width="400" height="250"></canvas>
                                            </Grid>
                                        </Grid>
                                        <Grid container className={classes.rootGrid} spacing={0}>
                                            <Grid item xs={12} sm={6} className={classNames(classes.marginauto, classes.textAlignCenter)}>
                                                <Typography variant="button" gutterBottom>
                                                    Memory
                                        </Typography>
                                                <canvas id={"MemoryPerf" + o.Id} width="400" height="250"></canvas>
                                            </Grid>
                                            <Grid item xs={12} sm={6} className={classNames(classes.marginauto, classes.textAlignCenter)}>
                                                <Typography variant="button" gutterBottom>
                                                    Disk
                                        </Typography>
                                                <canvas id={"DiskDrivePerf" + o.Id} width="400" height="250"></canvas>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                            </Grid>
                        ))
                    }
                </SwipeableViews>
            </div>
        );
    }
}

Home.displayName = 'Home';

Home.contextTypes = {
    router: PropTypes.object.isRequired,
};

Home.propsTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withWidth()(withStyles(HomeStyles)(Home));