import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SessionController from '../Controllers/SessionController';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import CompaniesStyles from '../Styles/CompaniesStyles';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import classNames from 'classnames';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { Input } from '@material-ui/core';
import Button from '@material-ui/core/Button';
class Companies extends Component {
    constructor(props) {
        super(props);

        this.state = {
            companie: "entreprise",
        };
    }
    


    componentDidMount() {
        SessionController.HandleConnectionComputer(() => {
            
        });

        SessionController.HandleConnectionComputerPerformance(() => {
            
        });
    }  

    componentDidUpdate(){
        
    }

    
    
     
    render() {   
        const { classes,  } = this.props;

        const companiesData = [
        {id:1, name:"Entreprise 1", localisation: "St naz"},
        {id:2, name:"Entreprise 2", localisation: "St naz"},
        {id:3, name:"Entreprise 3", localisation: "St naz"},
        {id:4, name:"Entreprise 4", localisation: "St naz"}
    ];
        

        
        return (
            <Paper className={classes.root} elevation={1} className={classNames(classes.paperCompanies)}>
                <Typography variant="h5" component="h3">Choix de l'entreprise a superviser</Typography>

                <form className={classes.root} autoComplete="off"></form>
                    <FormControl className={classes.formControl}></FormControl>
                    <InputLabel htmlFor="companie-select">Entreprise</InputLabel>
                    <Select
                    value={this.state.companie}
                    onChange={this.handleChange}
                    inputProps={{
                        name: 'companie',
                        id: 'companie-select',
                      }}
                    className={classes.selectCompanies}
                    >  
                   
                   {
                   companiesData.map((o, i) =>{
                       return(
                        <MenuItem value={o.name}>{o.name}</MenuItem>
                       )
                    })
                   }
                   </Select>    

                   <Button
                            variant="contained"
                            color="primary"
                            className={classes.buttonSubmit}
                            //onClick={this.handleSubmitCompanies}
                        >
                            Choisir
                    </Button>
            </Paper>
            

            
        );
    }
}


Companies.displayName = 'Companies';

Companies.contextTypes = {
    router: PropTypes.object.isRequired,
};

Companies.propsTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default (withStyles(CompaniesStyles)(Companies));