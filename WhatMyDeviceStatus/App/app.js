﻿import React from 'react';
import { render } from 'react-dom';
import AppMain from './AppMain';

(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;
        render(<AppMain />, document.getElementById('home'));  
    };
})();