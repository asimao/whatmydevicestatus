﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SessionController from './Controllers/SessionController';

class NotFound extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        SessionController.IsConnected = true;
        this.context.router.history.push("/")
    }

    render() {
        return (null);
    }
}

NotFound.displayName = 'NotFound';

NotFound.propTypes = {

};

export default NotFound;