import $ from 'jquery';
import swal from 'sweetalert2'


export function swaldismiss(text, type, promise) {
    var handler = function(e){
        if(document.getElementsByClassName("dismissSwal").length !== 0 && 
        (e.originalEvent.key === 'Enter' || e.originalEvent.code === 'Escape' || e.originalEvent.code === 'Space')){
            handlerswalclose(promise);
        }
    };

    function handlerswalclose (promise){
        swalclose(promise, handler);
    };

    swal({
        html: text,
        type: type,
        showConfirmButton: false,
        onOpen: function () {
            $(document.body).append(
                '<div class="dismissSwal" ' +
                'style="position:fixed; top:0; left:0; right:0; '+
                'bottom:0; background: transparent; z-index:2000"></div>'
            );
            $('.dismissSwal').on('click', function(){
                swalclose(promise, handler);
            });
            $(document).on('keydown', handler);
        },
        onClose: function () {
            swalclose(promise, handler);
        }
    });
}

export function swalpromise(title, text, type, promise) {
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#888888',
        cancelButtonColor: '#dddddd',
        cancelButtonText: 'No',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value && typeof promise === 'function') {
            promise();
        }
    });
}

export function swalclose(promise, handler) {
    swal.close();
    $('.dismissSwal') && $('.dismissSwal').remove();
    
    if(typeof promise == 'function')
        promise();

    if(typeof handler == 'function')
        $(document).off('keydown', handler);
}