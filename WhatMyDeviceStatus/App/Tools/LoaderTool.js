import $ from 'jquery'; 

export function load(message) {
    $('.mainloader').removeClass('hidden');
    $('.mainloadermessage').find('p').html(message);
}

export function unload(successFunction) {
    setTimeout(function () {
        $('.mainloader').addClass('hidden');
        $('.mainloadermessage').find('p').html("");
        if(typeof successFunction == 'function')
            successFunction();

        return true;
    }, 2000);
}