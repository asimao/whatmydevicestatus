var webpack = require('webpack');
var path = require('path');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');

function resolve(dir) {
    return path.join(__dirname, '..', dir)
}

module.exports = {
    //mode: 'production',
    mode: 'development',
    entry: [path.join(__dirname, 'App/app.js')],
    output: {
        path: path.join(__dirname, 'www/scripts'),
        filename: 'app.bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader',
            },
            {
                test: /\.html$|\.md$/,
                use: 'raw-loader',
            },
			{
                test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.woff2$|\.ttf$|\.eot$/,
                loader: 'url-loader',
            },
            {
                test: /^(?!.*\.{test,min}\.js$).*\.js$/,
                exclude: [nodeModulesPath],
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    resolve: {
        modules: [
            'node_modules'
        ],
    },
    plugins: [
        // new webpack.DefinePlugin({
        //     'process.env.NODE_ENV': '"production"'
        // })
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"development"'
        })
    ]
};