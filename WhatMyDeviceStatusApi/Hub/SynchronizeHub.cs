﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Linq;
using WhatMyDeviceStatusApi.Entities.Wmds;
using WhatMyDeviceStatusApi.Tools;

namespace WhatMyDeviceStatusApi
{
    public class SynchronizeHub : Hub
    {
        public void Init()
        {

        }

        public void SynchronizeCompanies()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();
            
            Clients.All.SendAsync(
                JsonConvert.SerializeObject(
                    wmdsc.Companies.Where(w => !w.Disabled).ToList(), JsonTool.JsonSetting()
                )
            );
        }

        public void SynchronizeUsers()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            Clients.All.SendAsync(
                JsonConvert.SerializeObject(
                    wmdsc.Users.Where(w => !w.Disabled).ToList(), JsonTool.JsonSetting()
                )
            );
        }
    }
}
