﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using WhatMyDeviceStatusApi.Entities.Wmds;
using WhatMyDeviceStatusApi.Tools;

namespace WhatMyDeviceStatusApi.Controllers
{

    public class ComputerController : Controller
    {
        private static Computer initialComputer = null;
        private static DateTime dtn;
        private static int computerId;
        private static IHubContext<SynchronizeHub> hub;

        public ComputerController(IHubContext<SynchronizeHub> hubcontext)
        {
            hub = hubcontext;
        }

        private static void SynchronizeComputer()
        {
            if(hub != null)
            {
                WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

                Computer computer = wmdsc.Computer
                    .Include(i => i.Antivirus)
                    .Include(i => i.ComputerPerfomance)
                    .Include(i => i.Cpu)
                    .Include(i => i.CurrentOperatingSystem)
                    .Include(i => i.DiskDrive)
                    .Include(i => i.Gpu)
                    .Include(i => i.NetworkCard)
                    .Include(i => i.Ram)
                    .Include(i => i.SoftwareInstalled)
                    .Include(i => i.UsbDevice)
                    .Where(w => w.Id == computerId).FirstOrDefault();

                hub.Clients.All.SendAsync("synchronizeComputer", JsonConvert.SerializeObject(computer, JsonTool.JsonSetting()));
            }
        }

        private static void SynchronizeComputerPerformance()
        {
            if (hub != null)
            {
                WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

                Computer computer = wmdsc.Computer
                    .Include(i => i.ComputerPerfomance)
                    .Where(w => w.Id == computerId).FirstOrDefault();

                hub.Clients.All.SendAsync("synchronizeComputerPerformance", JsonConvert.SerializeObject(computer, JsonTool.JsonSetting()));
            }
        }


        [HttpPost]
        public IActionResult SetComputerData(string computer)
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            try
            {
                initialComputer = JsonConvert.DeserializeObject<Computer>(computer);
                dtn = DateTime.Now;
                DisabledAllData();
            }
            catch (Exception e)
            {
                Computer existComputer = wmdsc.Computer.Where(w => w.Id == computerId).FirstOrDefault();
                if (existComputer != null)
                {
                    existComputer.LastUpdateError = e.Message;
                }
            }

            return Json(null);
        }


        [HttpPost]
        public IActionResult SetComputerPerfomanceData(string uuid, string computerPerfomance)
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();
            Computer existComputer = wmdsc.Computer.Where(w => w.Id == computerId).FirstOrDefault();
            List<ComputerPerfomance> computerPerfomances = new List<ComputerPerfomance>();

            try
            {
                existComputer = wmdsc.Computer.Where(w => w.Uuid == uuid).FirstOrDefault();
                if (existComputer != null)
                {
                    computerId = existComputer.Id;
                    dtn = DateTime.Now;
                    computerPerfomances.Add(JsonConvert.DeserializeObject<ComputerPerfomance>(computerPerfomance));
                    SetComputerPerfomance(computerPerfomances);
                }
            }
            catch (Exception e)
            {
                if (existComputer != null)
                {
                    existComputer.LastUpdateError = e.Message;
                }
            }



            return Json(null);
        }

        private void DisabledAllData()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            Computer computer = wmdsc.Computer.Where(w => w.Id == computerId).FirstOrDefault();
            if (computer != null)
            {
                computer.DisabledDate = dtn;
                computer.Disabled = true;
                computer.LastUpdateError = null;
                wmdsc.Computer.Update(computer);

                foreach (Antivirus antivirus in wmdsc.Antivirus)
                {
                    antivirus.DisabledDate = dtn;
                    antivirus.Disabled = true;
                    wmdsc.Antivirus.Update(antivirus);
                }

                foreach (Cpu cpu in wmdsc.Cpu)
                {
                    cpu.DisabledDate = dtn;
                    cpu.Disabled = true;
                    wmdsc.Cpu.Update(cpu);
                }

                foreach (CurrentOperatingSystem currentOperatingSystem in wmdsc.CurrentOperatingSystem)
                {
                    currentOperatingSystem.DisabledDate = dtn;
                    currentOperatingSystem.Disabled = true;
                    wmdsc.CurrentOperatingSystem.Update(currentOperatingSystem);
                }

                foreach (DiskDrive diskDrive in wmdsc.DiskDrive)
                {
                    diskDrive.DisabledDate = dtn;
                    diskDrive.Disabled = true;
                    wmdsc.DiskDrive.Update(diskDrive);
                }

                foreach (Gpu gpu in wmdsc.Gpu)
                {
                    gpu.DisabledDate = dtn;
                    gpu.Disabled = true;
                    wmdsc.Gpu.Update(gpu);
                }

                foreach (NetworkCard networkCard in wmdsc.NetworkCard)
                {
                    networkCard.DisabledDate = dtn;
                    networkCard.Disabled = true;
                    wmdsc.NetworkCard.Update(networkCard);
                }

                foreach (Ram ram in wmdsc.Ram)
                {
                    ram.DisabledDate = dtn;
                    ram.Disabled = true;
                    wmdsc.Ram.Update(ram);
                }

                foreach (SoftwareInstalled softwareInstalled in wmdsc.SoftwareInstalled)
                {
                    softwareInstalled.DisabledDate = dtn;
                    softwareInstalled.Disabled = true;
                    wmdsc.SoftwareInstalled.Update(softwareInstalled);
                }

                foreach (UsbDevice usbDevice in wmdsc.UsbDevice)
                {
                    usbDevice.DisabledDate = dtn;
                    usbDevice.Disabled = true;
                    wmdsc.UsbDevice.Update(usbDevice);
                }
            }

            SetComputer();
        }


        private void SetComputer()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            Computer existComputer = wmdsc.Computer.Where(w => w.Uuid == initialComputer.Uuid).FirstOrDefault();
            bool isAnUpdate = existComputer != null;


            Computer computer = isAnUpdate ? existComputer : new Computer();


            computer.IdentifyingNumber = initialComputer.IdentifyingNumber;
            computer.Uuid = initialComputer.Uuid;
            computer.Name = initialComputer.Name;
            computer.Vendor = initialComputer.Vendor;
            computer.LastUpdate = dtn;
            computer.DisabledDate = null;
            computer.Disabled = false;

            if (isAnUpdate)
            {
                wmdsc.Computer.Update(computer);
            }
            else
            {
                computer.AddDate = dtn;
                wmdsc.Computer.Add(computer);
            }


            wmdsc.SaveChanges();

            SetLog(computer.Id, 2, "Computer", null);

            computerId = computer.Id;

            SetAntivirus();
        }


        private void SetAntivirus()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            foreach (Antivirus icAntivirus in initialComputer.Antivirus)
            {
                Antivirus existAntivirus = wmdsc.Antivirus.Where(w => w.ComputerId == computerId && w.DisplayName == icAntivirus.DisplayName).FirstOrDefault();
                bool isAnUpdate = existAntivirus != null;

                Antivirus antivirus = isAnUpdate ? existAntivirus : new Antivirus();

                antivirus.ComputerId = computerId;
                antivirus.DisplayName = icAntivirus.DisplayName;
                antivirus.RealTimeProtectionStatus = icAntivirus.RealTimeProtectionStatus;
                antivirus.DefinitionStatus = icAntivirus.DefinitionStatus;
                antivirus.DisabledDate = null;
                antivirus.Disabled = false;

                if (isAnUpdate)
                {
                    wmdsc.Antivirus.Update(antivirus);
                    wmdsc.AntivirusSecurityProviderRelational.RemoveRange(wmdsc.AntivirusSecurityProviderRelational.Where(w => w.IdAntivirus == antivirus.Id));
                }
                else
                {
                    antivirus.AddDate = dtn;
                    wmdsc.Antivirus.Add(antivirus);
                }


                foreach (AntivirusSecurityProviderRelational aspr in icAntivirus.AntivirusSecurityProviderRelational)
                {
                    wmdsc.AntivirusSecurityProviderRelational.Add(new AntivirusSecurityProviderRelational()
                    {
                        IdAntivirus = antivirus.Id,
                        IdAntivirusSecurityProvider = aspr.IdAntivirusSecurityProvider
                    });
                }

                wmdsc.SaveChanges();

                SetLog(antivirus.Id, 2, "Antivirus", null);
            }

            SetComputerPerfomance();
        }

        private void SetComputerPerfomance(List<ComputerPerfomance> computerPerfomances = null)
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            foreach (ComputerPerfomance computerPerfomance in (computerPerfomances ?? initialComputer.ComputerPerfomance))
            {
                ComputerPerfomance cp = new ComputerPerfomance()
                {
                    ComputerId = computerId,
                    DiskDrivePerf = computerPerfomance.DiskDrivePerf,
                    CpuPerf = computerPerfomance.CpuPerf,
                    MemoryPerf = computerPerfomance.MemoryPerf,
                    LastUpdate = dtn,
                };

                wmdsc.ComputerPerfomance.Add(cp);

                wmdsc.SaveChanges();

                //SetLog(cp.Id, 2, "ComputerPerfomance", null);
            }

            if (computerPerfomances == null)
            {
                SetCpu();
            }
            else
            {
                SynchronizeComputerPerformance();
            }
        }

        private void SetCpu()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            foreach (Cpu cpu in initialComputer.Cpu)
            {
                Cpu existCpu = wmdsc.Cpu.Where(w => w.ComputerId == computerId && w.Name == cpu.Name).FirstOrDefault();
                bool isAnUpdate = existCpu != null;

                Cpu processor = isAnUpdate ? existCpu : new Cpu();

                processor.ComputerId = computerId;
                processor.ClockSpeed = cpu.ClockSpeed;
                processor.Name = cpu.Name;
                processor.Computer = cpu.Computer;
                processor.DisabledDate = null;
                processor.Disabled = false;

                if (isAnUpdate)
                {
                    wmdsc.Cpu.Update(processor);

                }
                else
                {
                    processor.AddDate = dtn;
                    wmdsc.Cpu.Add(processor);
                }

                wmdsc.SaveChanges();

                SetLog(processor.Id, 2, "Cpu", null);
            }

            SetCurrentOperatingSystem();
        }



        private void SetCurrentOperatingSystem()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            foreach (CurrentOperatingSystem os in initialComputer.CurrentOperatingSystem)
            {

                CurrentOperatingSystem existCurrentOperatingSystem = wmdsc.CurrentOperatingSystem.Where(w => w.ComputerId == computerId && w.Caption == os.Caption).FirstOrDefault();
                bool isAnUpdate = existCurrentOperatingSystem != null;

                CurrentOperatingSystem cos = isAnUpdate ? existCurrentOperatingSystem : new CurrentOperatingSystem();

                cos.ComputerId = computerId;
                cos.BuildNumber = os.BuildNumber;
                cos.Caption = os.Caption;
                cos.Osarchitecture = os.Osarchitecture;
                cos.Computer = os.Computer;
                cos.DisabledDate = null;
                cos.Disabled = false;

                if (isAnUpdate)
                {
                    wmdsc.CurrentOperatingSystem.Update(cos);

                }
                else
                {
                    cos.AddDate = dtn;
                    wmdsc.CurrentOperatingSystem.Add(cos);
                }

                wmdsc.SaveChanges();

                SetLog(cos.Id, 2, "CurrentOperatingSystem", null);
            }


            SetDiskDrive();
        }


        private void SetDiskDrive()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            foreach (DiskDrive dd in initialComputer.DiskDrive)
            {
                DiskDrive existDiskDrive = wmdsc.DiskDrive.Where(w => w.ComputerId == computerId && w.DiskName == dd.DiskName).FirstOrDefault();
                bool isAnUpdate = existDiskDrive != null;

                DiskDrive ddd = isAnUpdate ? existDiskDrive : new DiskDrive();

                ddd.ComputerId = computerId;
                ddd.PhysicalName = dd.PhysicalName;
                ddd.DiskName = dd.DiskName;
                ddd.DiskModel = dd.DiskModel;
                ddd.DiskInterface = dd.DiskInterface;
                ddd.Capabilities = dd.Capabilities;
                ddd.MediaLoaded = dd.MediaLoaded;
                ddd.MediaType = dd.MediaType;
                ddd.MediaSignature = dd.MediaSignature;
                ddd.MediaStatus = dd.MediaStatus;
                ddd.DriveName = dd.DriveName;
                ddd.DriveId = dd.DriveId;
                ddd.DriveCompressed = dd.DriveCompressed;
                ddd.DriveType = dd.DriveType;
                ddd.FreeSpace = dd.FreeSpace;
                ddd.TotalSpace = dd.TotalSpace;
                ddd.DriveMediaType = dd.DriveMediaType;
                ddd.VolumeName = dd.VolumeName;
                ddd.VolumeSerialNumber = dd.VolumeSerialNumber;
                ddd.FileSystem = dd.FileSystem;
                ddd.DisabledDate = null;
                ddd.Disabled = false;

                if (isAnUpdate)
                {
                    wmdsc.DiskDrive.Update(ddd);

                }
                else
                {
                    ddd.AddDate = dtn;
                    wmdsc.DiskDrive.Add(ddd);
                }

                wmdsc.SaveChanges();

                SetLog(ddd.Id, 2, "DiskDrive", null);
            }

            SetGpu();
        }

        private void SetGpu()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            foreach (Gpu gpu in initialComputer.Gpu)
            {
                Gpu existGpu = wmdsc.Gpu.Where(w => w.ComputerId == computerId && w.Description == gpu.Description).FirstOrDefault();
                bool isAnUpdate = existGpu != null;

                Gpu gpuu = isAnUpdate ? existGpu : new Gpu();

                gpuu.ComputerId = computerId;
                gpuu.Description = gpu.Description;
                gpuu.DriverDate = gpu.DriverDate;
                gpuu.DisabledDate = null;
                gpuu.Disabled = false;

                if (isAnUpdate)
                {
                    wmdsc.Gpu.Update(gpuu);
                }
                else
                {
                    gpuu.AddDate = dtn;
                    wmdsc.Gpu.Add(gpuu);
                }

                wmdsc.SaveChanges();

                SetLog(gpuu.Id, 2, "Gpu", null);
            }

            SetNetworkCards();
        }

        private void SetNetworkCards()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            foreach (NetworkCard nc in initialComputer.NetworkCard)
            {
                NetworkCard existNetworkCard = wmdsc.NetworkCard.Where(w => w.ComputerId == computerId && w.Description == nc.Description).FirstOrDefault();
                bool isAnUpdate = existNetworkCard != null;

                NetworkCard netCard = isAnUpdate ? existNetworkCard : new NetworkCard();

                netCard.ComputerId = computerId;
                netCard.Macaddress = nc.Macaddress;
                netCard.Description = nc.Description;
                netCard.Guid = nc.Guid;
                netCard.Netconnectionstatus = nc.Netconnectionstatus;
                netCard.Speed = nc.Speed;
                netCard.DisabledDate = null;
                netCard.Disabled = false;

                if (isAnUpdate)
                {
                    wmdsc.NetworkCard.Update(netCard);
                }
                else
                {
                    netCard.AddDate = dtn;
                    wmdsc.NetworkCard.Add(netCard);
                }

                wmdsc.SaveChanges();

                SetLog(netCard.Id, 2, "NetworkCard", null);
            }

            SetRandomAccesMemory();
        }

        private void SetRandomAccesMemory()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();
            wmdsc.RemoveRange(wmdsc.Ram.Where(w => w.ComputerId == computerId));

            foreach (Ram ram in initialComputer.Ram)
            {
                Ram existRam = wmdsc.Ram.Where(w => w.ComputerId == computerId &&
                w.BankLabel == ram.BankLabel && w.Speed == ram.Speed &&
                w.Capacity == ram.Capacity).FirstOrDefault();

                bool isAnUpdate = existRam != null;

                Ram rams = new Ram()
                {
                    ComputerId = computerId,
                    Speed = ram.Speed,
                    BankLabel = ram.BankLabel,
                    Capacity = ram.Capacity,
                    AddDate = dtn,
                    DisabledDate = null,
                    Disabled = false
                };

                wmdsc.Ram.Add(rams);

                wmdsc.SaveChanges();

                SetLog(rams.Id, 2, "Ram", null);
            }

            SetSoftwareInstalled();
        }

        private void SetSoftwareInstalled()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            foreach (SoftwareInstalled sis in initialComputer.SoftwareInstalled)
            {
                SoftwareInstalled existSoftwareInstalled = wmdsc.SoftwareInstalled
                    .Where(w => w.ComputerId == computerId &&
                        w.Name == sis.Name).FirstOrDefault();

                bool isAnUpdate = existSoftwareInstalled != null;

                SoftwareInstalled siss = isAnUpdate ? existSoftwareInstalled : new SoftwareInstalled();

                siss.ComputerId = computerId;
                siss.Name = sis.Name;
                siss.Version = sis.Version;
                siss.InstallDate = sis.InstallDate;
                siss.Vendor = sis.Vendor;
                siss.DisplayName = sis.DisplayName;
                siss.ProductState = sis.ProductState;
                siss.DisabledDate = null;
                siss.Disabled = false;

                if (isAnUpdate)
                {
                    wmdsc.SoftwareInstalled.Update(siss);
                }
                else
                {
                    siss.AddDate = dtn;
                    wmdsc.SoftwareInstalled.Add(siss);
                }

                wmdsc.SaveChanges();

                SetLog(siss.Id, 2, "SoftwareInstalled", null);
            }

            SetUsbDevice();
        }


        private void SetUsbDevice()
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            foreach (UsbDevice ud in initialComputer.UsbDevice)
            {
                UsbDevice existUsbDevice = wmdsc.UsbDevice
                    .Where(w => w.ComputerId == computerId &&
                        w.Caption == ud.Caption).FirstOrDefault();

                bool isAnUpdate = existUsbDevice != null;

                UsbDevice udd = isAnUpdate ? existUsbDevice : new UsbDevice();

                udd.ComputerId = computerId;
                udd.Status = ud.Status;
                udd.Caption = ud.Caption;
                udd.DisabledDate = null;
                udd.Disabled = false;

                if (isAnUpdate)
                {
                    wmdsc.UsbDevice.Update(udd);
                }
                else
                {
                    udd.AddDate = dtn;
                    wmdsc.UsbDevice.Add(udd);
                }

                wmdsc.SaveChanges();

                SetLog(udd.Id, 2, "UsbDevice", null);
            }

            SynchronizeComputer();
        }


        private void SetAlerts(bool isAnUpdate, int type, string data)
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();


        }


        private void SetLog(int idData, int type, string dataType, string device)
        {
            WhatMyDeviceStatusContext wmdsc = new WhatMyDeviceStatusContext();

            wmdsc.Alerts.Add(new Alerts()
            {
                ComputerId = computerId,
                Adddate = dtn,
                AlertsTypeId = type,
                DataId = idData,
                DataType = dataType,
                Device = device
            });

            wmdsc.SaveChanges();
        }
    }
}