﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class Companies
    {
        public Companies()
        {
            Users = new HashSet<Users>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Disabled { get; set; }

        public virtual ICollection<Users> Users { get; set; }
    }
}
