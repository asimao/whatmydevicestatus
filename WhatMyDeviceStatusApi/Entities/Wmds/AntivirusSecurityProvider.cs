﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class AntivirusSecurityProvider
    {
        public AntivirusSecurityProvider()
        {
            AntivirusSecurityProviderRelational = new HashSet<AntivirusSecurityProviderRelational>();
        }

        public int Id { get; set; }
        public string Data { get; set; }

        public virtual ICollection<AntivirusSecurityProviderRelational> AntivirusSecurityProviderRelational { get; set; }
    }
}
