﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class Computer
    {
        public Computer()
        {
            Alerts = new HashSet<Alerts>();
            Antivirus = new HashSet<Antivirus>();
            ComputerPerfomance = new HashSet<ComputerPerfomance>();
            Cpu = new HashSet<Cpu>();
            CurrentOperatingSystem = new HashSet<CurrentOperatingSystem>();
            DiskDrive = new HashSet<DiskDrive>();
            Gpu = new HashSet<Gpu>();
            NetworkCard = new HashSet<NetworkCard>();
            Ram = new HashSet<Ram>();
            SoftwareInstalled = new HashSet<SoftwareInstalled>();
            UsbDevice = new HashSet<UsbDevice>();
        }

        public int Id { get; set; }
        public string IdentifyingNumber { get; set; }
        public string Uuid { get; set; }
        public string Name { get; set; }
        public string Vendor { get; set; }
        public DateTime AddDate { get; set; }
        public string LastUpdateError { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime? DisabledDate { get; set; }
        public bool Disabled { get; set; }
        public int? IdUser { get; set; }

        public virtual Users IdUserNavigation { get; set; }
        public virtual ICollection<Alerts> Alerts { get; set; }
        public virtual ICollection<Antivirus> Antivirus { get; set; }
        public virtual ICollection<ComputerPerfomance> ComputerPerfomance { get; set; }
        public virtual ICollection<Cpu> Cpu { get; set; }
        public virtual ICollection<CurrentOperatingSystem> CurrentOperatingSystem { get; set; }
        public virtual ICollection<DiskDrive> DiskDrive { get; set; }
        public virtual ICollection<Gpu> Gpu { get; set; }
        public virtual ICollection<NetworkCard> NetworkCard { get; set; }
        public virtual ICollection<Ram> Ram { get; set; }
        public virtual ICollection<SoftwareInstalled> SoftwareInstalled { get; set; }
        public virtual ICollection<UsbDevice> UsbDevice { get; set; }
    }
}
