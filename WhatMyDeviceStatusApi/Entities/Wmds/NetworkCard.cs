﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class NetworkCard
    {
        public int Id { get; set; }
        public int ComputerId { get; set; }
        public string Macaddress { get; set; }
        public string Description { get; set; }
        public string Guid { get; set; }
        public int Netconnectionstatus { get; set; }
        public int Speed { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime? DisabledDate { get; set; }
        public bool Disabled { get; set; }

        public virtual Computer Computer { get; set; }
    }
}
