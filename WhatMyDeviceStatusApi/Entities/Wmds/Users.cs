﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class Users
    {
        public Users()
        {
            Computer = new HashSet<Computer>();
        }

        public int Id { get; set; }
        public int? IdCompanie { get; set; }
        public string LastName { get; set; }
        public string FisrtName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool Disabled { get; set; }

        public virtual Companies IdCompanieNavigation { get; set; }
        public virtual ICollection<Computer> Computer { get; set; }
    }
}
