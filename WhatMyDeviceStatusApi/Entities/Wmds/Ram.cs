﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class Ram
    {
        public int Id { get; set; }
        public int ComputerId { get; set; }
        public string Speed { get; set; }
        public string BankLabel { get; set; }
        public string Capacity { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime? DisabledDate { get; set; }
        public bool Disabled { get; set; }

        public virtual Computer Computer { get; set; }
    }
}
