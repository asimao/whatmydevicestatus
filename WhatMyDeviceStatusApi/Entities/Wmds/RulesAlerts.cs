﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class RulesAlerts
    {
        public int Id { get; set; }
        public string Source { get; set; }
        public int Rule { get; set; }
        public int AlertTypeId { get; set; }
    }
}
