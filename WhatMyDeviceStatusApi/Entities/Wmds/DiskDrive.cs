﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class DiskDrive
    {
        public int Id { get; set; }
        public int ComputerId { get; set; }
        public string PhysicalName { get; set; }
        public string DiskName { get; set; }
        public string DiskModel { get; set; }
        public string DiskInterface { get; set; }
        public string Capabilities { get; set; }
        public string MediaLoaded { get; set; }
        public string MediaType { get; set; }
        public string MediaSignature { get; set; }
        public string MediaStatus { get; set; }
        public string DriveName { get; set; }
        public string DriveId { get; set; }
        public string DriveCompressed { get; set; }
        public string DriveType { get; set; }
        public string FreeSpace { get; set; }
        public string TotalSpace { get; set; }
        public string DriveMediaType { get; set; }
        public string VolumeName { get; set; }
        public string VolumeSerialNumber { get; set; }
        public string FileSystem { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime? DisabledDate { get; set; }
        public bool Disabled { get; set; }

        public virtual Computer Computer { get; set; }
    }
}
