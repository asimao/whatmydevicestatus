﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class AntivirusRealTimeProtectionStatus
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }
}
