﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class AntivirusSecurityProviderRelational
    {
        public int Id { get; set; }
        public int IdAntivirus { get; set; }
        public int IdAntivirusSecurityProvider { get; set; }

        public virtual Antivirus IdAntivirusNavigation { get; set; }
        public virtual AntivirusSecurityProvider IdAntivirusSecurityProviderNavigation { get; set; }
    }
}
