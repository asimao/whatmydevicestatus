﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class Alerts
    {
        public int Id { get; set; }
        public int RulesAlertsId { get; set; }
        public int ComputerId { get; set; }
        public string DataType { get; set; }
        public DateTime Adddate { get; set; }
        public DateTime? DisabledDate { get; set; }
        public bool Disabled { get; set; }
        public int AlertsTypeId { get; set; }
        public int DataId { get; set; }
        public string Device { get; set; }

        public virtual AlertsType AlertsType { get; set; }
        public virtual Computer Computer { get; set; }
    }
}
