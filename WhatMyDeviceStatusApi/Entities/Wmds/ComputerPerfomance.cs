﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class ComputerPerfomance
    {
        public int Id { get; set; }
        public int ComputerId { get; set; }
        public double DiskDrivePerf { get; set; }
        public double MemoryPerf { get; set; }
        public double CpuPerf { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool Disabled { get; set; }

        public virtual Computer Computer { get; set; }
    }
}
