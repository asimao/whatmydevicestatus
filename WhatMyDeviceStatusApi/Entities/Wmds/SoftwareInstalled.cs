﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class SoftwareInstalled
    {
        public int Id { get; set; }
        public int ComputerId { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
        public string InstallDate { get; set; }
        public string Vendor { get; set; }
        public string DisplayName { get; set; }
        public string ProductState { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime? DisabledDate { get; set; }
        public bool Disabled { get; set; }

        public virtual Computer Computer { get; set; }
    }
}
