﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class AlertsType
    {
        public AlertsType()
        {
            Alerts = new HashSet<Alerts>();
        }

        public int Id { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Alerts> Alerts { get; set; }
    }
}
