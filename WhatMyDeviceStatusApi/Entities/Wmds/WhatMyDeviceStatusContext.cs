﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class WhatMyDeviceStatusContext : DbContext
    {
        public WhatMyDeviceStatusContext()
        {
        }

        public WhatMyDeviceStatusContext(DbContextOptions<WhatMyDeviceStatusContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Alerts> Alerts { get; set; }
        public virtual DbSet<AlertsType> AlertsType { get; set; }
        public virtual DbSet<Antivirus> Antivirus { get; set; }
        public virtual DbSet<AntivirusDefinitionStatus> AntivirusDefinitionStatus { get; set; }
        public virtual DbSet<AntivirusRealTimeProtectionStatus> AntivirusRealTimeProtectionStatus { get; set; }
        public virtual DbSet<AntivirusSecurityProvider> AntivirusSecurityProvider { get; set; }
        public virtual DbSet<AntivirusSecurityProviderRelational> AntivirusSecurityProviderRelational { get; set; }
        public virtual DbSet<Companies> Companies { get; set; }
        public virtual DbSet<Computer> Computer { get; set; }
        public virtual DbSet<ComputerPerfomance> ComputerPerfomance { get; set; }
        public virtual DbSet<Cpu> Cpu { get; set; }
        public virtual DbSet<CurrentOperatingSystem> CurrentOperatingSystem { get; set; }
        public virtual DbSet<DiskDrive> DiskDrive { get; set; }
        public virtual DbSet<Gpu> Gpu { get; set; }
        public virtual DbSet<NetworkCard> NetworkCard { get; set; }
        public virtual DbSet<Ram> Ram { get; set; }
        public virtual DbSet<RulesAlerts> RulesAlerts { get; set; }
        public virtual DbSet<SoftwareInstalled> SoftwareInstalled { get; set; }
        public virtual DbSet<UsbDevice> UsbDevice { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.1-servicing-10028");

            modelBuilder.Entity<Alerts>(entity =>
            {
                entity.Property(e => e.Adddate).HasColumnType("datetime");

                entity.Property(e => e.DataType)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Device).IsUnicode(false);

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.HasOne(d => d.AlertsType)
                    .WithMany(p => p.Alerts)
                    .HasForeignKey(d => d.AlertsTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Alerts_AlertsType");

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.Alerts)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Alerts_Computer");
            });

            modelBuilder.Entity<AlertsType>(entity =>
            {
                entity.Property(e => e.Type)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Antivirus>(entity =>
            {
                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.Antivirus)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Antivirus_Computer");
            });

            modelBuilder.Entity<AntivirusDefinitionStatus>(entity =>
            {
                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AntivirusRealTimeProtectionStatus>(entity =>
            {
                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AntivirusSecurityProvider>(entity =>
            {
                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AntivirusSecurityProviderRelational>(entity =>
            {
                entity.HasOne(d => d.IdAntivirusNavigation)
                    .WithMany(p => p.AntivirusSecurityProviderRelational)
                    .HasForeignKey(d => d.IdAntivirus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AntivirusSecurityProviderRelational_Antivirus");

                entity.HasOne(d => d.IdAntivirusSecurityProviderNavigation)
                    .WithMany(p => p.AntivirusSecurityProviderRelational)
                    .HasForeignKey(d => d.IdAntivirusSecurityProvider)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AntivirusSecurityProviderRelational_AntivirusSecurityProvider");
            });

            modelBuilder.Entity<Companies>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Computer>(entity =>
            {
                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.Property(e => e.IdentifyingNumber)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdate).HasColumnType("datetime");

                entity.Property(e => e.LastUpdateError)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .IsUnicode(false);

                entity.Property(e => e.Vendor)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Computer)
                    .HasForeignKey(d => d.IdUser)
                    .HasConstraintName("FK_Computer_Users");
            });

            modelBuilder.Entity<ComputerPerfomance>(entity =>
            {
                entity.Property(e => e.LastUpdate).HasColumnType("datetime");

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.ComputerPerfomance)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ComputerPerfomance_Computer");
            });

            modelBuilder.Entity<Cpu>(entity =>
            {
                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.ClockSpeed)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.Cpu)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cpu_Computer");
            });

            modelBuilder.Entity<CurrentOperatingSystem>(entity =>
            {
                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.BuildNumber)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Caption)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.Property(e => e.Osarchitecture)
                    .IsRequired()
                    .HasColumnName("OSArchitecture")
                    .IsUnicode(false);

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.CurrentOperatingSystem)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CurrentOperatingSystem_Computer");
            });

            modelBuilder.Entity<DiskDrive>(entity =>
            {
                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.Capabilities)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.Property(e => e.DiskInterface)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DiskModel)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DiskName)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DriveCompressed)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DriveId).IsUnicode(false);

                entity.Property(e => e.DriveMediaType)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DriveName).IsUnicode(false);

                entity.Property(e => e.DriveType)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.FileSystem)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.FreeSpace)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.MediaLoaded)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.MediaSignature)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.MediaStatus)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.MediaType)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.PhysicalName)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.TotalSpace)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.VolumeName).IsUnicode(false);

                entity.Property(e => e.VolumeSerialNumber)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.DiskDrive)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DiskDrive_Computer");
            });

            modelBuilder.Entity<Gpu>(entity =>
            {
                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.Property(e => e.DriverDate)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.Gpu)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Gpu_Computer");
            });

            modelBuilder.Entity<NetworkCard>(entity =>
            {
                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.Property(e => e.Guid)
                    .HasColumnName("GUID")
                    .IsUnicode(false);

                entity.Property(e => e.Macaddress)
                    .HasColumnName("MACAddress")
                    .IsUnicode(false);

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.NetworkCard)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NetworkCard_Computer");
            });

            modelBuilder.Entity<Ram>(entity =>
            {
                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.BankLabel)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Capacity)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.Property(e => e.Speed)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.Ram)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Ram_Computer");
            });

            modelBuilder.Entity<RulesAlerts>(entity =>
            {
                entity.Property(e => e.Source)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SoftwareInstalled>(entity =>
            {
                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.Property(e => e.DisplayName).IsUnicode(false);

                entity.Property(e => e.InstallDate)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ProductState).IsUnicode(false);

                entity.Property(e => e.Vendor)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.SoftwareInstalled)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SoftwareInstalled_Computer");
            });

            modelBuilder.Entity<UsbDevice>(entity =>
            {
                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.Caption)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DisabledDate).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.Computer)
                    .WithMany(p => p.UsbDevice)
                    .HasForeignKey(d => d.ComputerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UsbDevice_Computer");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.FisrtName)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber).IsUnicode(false);

                entity.HasOne(d => d.IdCompanieNavigation)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.IdCompanie)
                    .HasConstraintName("FK_Users_Companies");
            });
        }
    }
}
