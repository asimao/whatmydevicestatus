﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class Antivirus
    {
        public Antivirus()
        {
            AntivirusSecurityProviderRelational = new HashSet<AntivirusSecurityProviderRelational>();
        }

        public int Id { get; set; }
        public int ComputerId { get; set; }
        public string DisplayName { get; set; }
        public int RealTimeProtectionStatus { get; set; }
        public int DefinitionStatus { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime? DisabledDate { get; set; }
        public bool Disabled { get; set; }

        public virtual Computer Computer { get; set; }
        public virtual ICollection<AntivirusSecurityProviderRelational> AntivirusSecurityProviderRelational { get; set; }
    }
}
