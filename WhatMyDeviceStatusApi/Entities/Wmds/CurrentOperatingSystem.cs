﻿using System;
using System.Collections.Generic;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class CurrentOperatingSystem
    {
        public int Id { get; set; }
        public int ComputerId { get; set; }
        public string Caption { get; set; }
        public string Osarchitecture { get; set; }
        public string BuildNumber { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime? DisabledDate { get; set; }
        public bool Disabled { get; set; }

        public virtual Computer Computer { get; set; }
    }
}
