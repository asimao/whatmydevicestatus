﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WhatMyDeviceStatusApi.Tools;

namespace WhatMyDeviceStatusApi.Entities.Wmds
{
    public partial class WhatMyDeviceStatusContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(ConnectionTool.GetDbContext());
            }
            
        }
    }
}
