# WhatMyDeviceStatus

WhatMyDeviceStatus est un projet de solution de supervision fonctionnant sous Windows réalisé dans le cadre d'un BTS SNIR en alternance.
Elle a pour but d'être une solution complète composé d'un agent, d'une Api et d'un frontEnd 

# Agent Wmds

L'agent WMDS est développé en **C#** sous forme de service Windows cela lui permet d'être demarré automatiquement au démarrage de Windows
Les objets ont été généré grâce a l'ORM Entity Framework 6.2.x 

## Installation de l'agent 

L'agent n'a pour le moment pas d'installateur qui lui est dédié il faut donc modifier puis exécuté un script présent dans le répertoire

Pour installer l'agent il est nécessaire de démarrer le PC en mode administrateur 

    whatmydevicestatus\WhatMyDeviceStatusAgent\bin
   
   Contenu du Batch d'installation

    set pathOfInstallTools = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe"
    
    set pathOfWmdsAgent = "Chemin vers Debug\WhatMyDeviceStatusAgent.exe"
    
    
    
    %pathOfInstallTools% %pathOfWmdsAgent%
    
    sc start "Wmds"
    pause

## Désinstallation de l'agent

pas d'installateur pour le moment donc pas de désinstallateur il faut le faire  grâce au script  "UninstallService.bat" présent dans le répertoire :
    

> whatmydevicestatus\WhatMyDeviceStatusAgent\bin


## Installation du frontEnd

### Installation du frontEnd en mode "Developpement"

La solution WMDS n’étant pas fini les frontEnd tourne dans un conteneur Docker il faut donc que Docker soit installé sur la machine cible.
Il faut se rendre dans le dossier
> whatmydevicestatus\WhatMyDeviceStatusAgent\WhatMyDeviceStatus

Puis lancer 3 invite de commande Windows pour démarrer l'interface

La première  

> npm install 

La seconde 

> docker-compose up --build 

La troisième    

> npm run app

Une fois ces 3 commandes faites il vous est possible d'accéder à l’interface web à l’aide d’un navigateur et de l’adresse Localhost (ou 127.0.0.1)
