﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using WhatMyDeviceStatusAgent.Controllers;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent.Controllers
{
    public class ComputerController
    {

        public static Computer GetComputer()
        {

            Computer computer = GetComputerInformation();
            computer.Ram = RamController.GetRams();
            computer.UsbDevice = UsbDeviceController.GetUsbDevices();
            computer.Cpu = CpuController.GetCpus();
            computer.DiskDrive = DiskDriveController.GetDiskDrives();
            computer.Gpu = GpuController.GetGpus();
            computer.NetworkCard = NetworkCardController.GetNetworkCards();
            computer.SoftwareInstalled = SoftwareInstalledController.GetSoftwareInstalled();
            computer.Antivirus = AntivirusController.GetAntivirusInstalled();
            computer.CurrentOperatingSystem.Add(CurrentOperatingSystemController.GetCurrentOperatingSystem());
            computer.ComputerPerfomance.Add(ComputerPerfomanceController.GetComputerPerfomance());
            
            return computer;
        }

        private static List<string> keys = new List<string>() { "IdentifyingNumber", "UUID", "Name", "Vendor" };
        private static string table = "Win32_ComputerSystemProduct";
        private static string path = @"\\localhost\ROOT\CIMV2";

        public static Computer GetComputerInformation()
        {
            List<ManagementObject> collection = WmiTool.GetWmiData(path, WmiTool.GetQuery(keys, table));
            Computer computer = new Computer();

            if (collection != null && collection.Count > 0)
            {

                foreach (ManagementObject oneObjectCollection in collection)
                {

                    foreach (string s in keys)
                    {
                        if (WmiTool.CollectionWmiHaveValue(oneObjectCollection, s))
                        {
                            computer = SetComputer(computer, s, oneObjectCollection[s].ToString());

                        }
                    }

                }

            }

            return computer;
        }

        private static Computer SetComputer(Computer computer, string key, string value)
        {
            switch (key)
            {
                case "IdentifyingNumber":
                    computer.IdentifyingNumber = value;
                    break;
                case "UUID":
                    computer.UUID = value;
                    break;
                case "Name":
                    computer.Name = value;
                    break;
                case "Vendor":
                    computer.Vendor = value;
                    break;

                default:
                    break;
            }

            return computer;
        }



    }
}
