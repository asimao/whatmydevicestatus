﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent.Controllers
{
    public class RamController
    {
        private static List<string> keys = new List<string>() { "Speed", "BankLabel", "Capacity" };
        private static string table = "Win32_PhysicalMemory";
        private static string path = @"\\localhost\ROOT\CIMV2";


        public static List<Ram> GetRams()
        {
            List<ManagementObject> collection = WmiTool.GetWmiData(path, WmiTool.GetQuery(keys, table));
            List<Ram> rams = new List<Ram>();
            
            if (collection != null && collection.Count > 0)
            {
                foreach (ManagementObject oneObjectCollection in collection)
                {
                    Ram ram = new Ram();
                    foreach (string s in keys)
                    {
                        if (WmiTool.CollectionWmiHaveValue(oneObjectCollection, s))
                        {
                            SetRam(ram, s, oneObjectCollection[s].ToString());
                        }
                    }

                    rams.Add(ram);
                }
            }

            return rams;
        }

        public static Ram SetRam(Ram ram, string key, string value)
        {
            switch (key)
            {
                case "Speed":
                    ram.Speed = value;
                    break;
                case "BankLabel":
                    ram.BankLabel = value;
                    break;
                case "Capacity":
                    ram.Capacity = value;
                    break;
                default:
                    break;
            }

            return ram;
        }
    }
}