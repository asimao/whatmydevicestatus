﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent.Controllers
{
    public class CurrentOperatingSystemController
    {
        private static List<string> keys = new List<string>() { "Caption", "OSArchitecture", "BuildNumber" };
        private static string table = "Win32_OperatingSystem";
        private static string path = @"\\localhost\ROOT\CIMV2";


        public static CurrentOperatingSystem GetCurrentOperatingSystem()
        {
            List<ManagementObject> collection = WmiTool.GetWmiData(path, WmiTool.GetQuery(keys, table));
            // Models.OperatingSystem operatingSystems = new Models.OperatingSystem();
            CurrentOperatingSystem currentOperatingSystem = new CurrentOperatingSystem();

            if (collection != null && collection.Count > 0)
            {

                foreach (ManagementObject oneObjectCollection in collection)
                {
                    
                    foreach (string s in keys)
                    {
                        if (WmiTool.CollectionWmiHaveValue(oneObjectCollection, s))
                        {
                            currentOperatingSystem = SetCurrentOperatingSystem(currentOperatingSystem, s, oneObjectCollection[s].ToString());
                            
                        }
                    }
                    
                }
                
            }
            return currentOperatingSystem;
        }

        private static CurrentOperatingSystem SetCurrentOperatingSystem(CurrentOperatingSystem CurrentOperatingSystem, string key, string value)
        {
            switch (key)
            {
                case "Caption":
                    CurrentOperatingSystem.Caption = value;
                    break;
                case "OSArchitecture":
                    CurrentOperatingSystem.OSArchitecture = value;
                    break;
                case "BuildNumber":
                    CurrentOperatingSystem.BuildNumber = value;
                    break;

                default:
                    break;
            }

            return CurrentOperatingSystem;
        }
    }
}
    
