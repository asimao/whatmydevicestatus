﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent.Controllers
{
    class CpuController
    {
        private static List<string> keys = new List<string>() { "Name", "CurrentClockSpeed" };
        private static string table = "Win32_Processor";
        private static string path = @"\\localhost\ROOT\CIMV2";

        public static List<Cpu> GetCpus()
        {
            List<ManagementObject> collection = WmiTool.GetWmiData(path, WmiTool.GetQuery(keys, table));
            List<Cpu> cpus = new List<Cpu>();

            if (collection != null && collection.Count > 0)
            {
                foreach (ManagementObject oneObjectCollection in collection)
                {
                    Cpu cpu = new Cpu();
                    foreach (string s in keys)
                    {
                        if (WmiTool.CollectionWmiHaveValue(oneObjectCollection, s))
                        {
                            SetCpu(cpu, s, oneObjectCollection[s].ToString());
                        }
                    }

                    cpus.Add(cpu);
                }
            }

            return cpus;
        }

        public static Cpu SetCpu(Cpu cpu, string key, string value)
        {
            switch (key)
            {
                case "Name":
                    cpu.Name = value;
                    break;
                case "CurrentClockSpeed":
                    cpu.ClockSpeed = value;
                    break;

                default:
                    break;
            }

            return cpu;
        }


    }
}
