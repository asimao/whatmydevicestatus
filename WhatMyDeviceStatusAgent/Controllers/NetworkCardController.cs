﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent.Controllers
{
    class NetworkCardController
    {
        private static List<string> keys = new List<string>() { "MACAddress", "Description", "GUID", "Netconnectionstatus", "Speed" };
        private static string table = "Win32_NetworkAdapter";
        private static string path = @"\\localhost\ROOT\CIMV2";


        public static List<NetworkCard> GetNetworkCards()
        {
            List<ManagementObject> collection = WmiTool.GetWmiData(path, WmiTool.GetQuery(keys, table));
            List<NetworkCard> networkCards = new List<NetworkCard>();

            if (collection != null && collection.Count > 0)
            {
                foreach (ManagementObject oneObjectCollection in collection)
                {
                    NetworkCard networkCard = new NetworkCard();
                    foreach (string s in keys)
                    {
                        if (WmiTool.CollectionWmiHaveValue(oneObjectCollection, s))
                        {
                            SetNetworkCard(networkCard, s, oneObjectCollection[s].ToString());
                        }
                    }

                    networkCards.Add(networkCard);
                }
            }

            return networkCards;
        }

        public static NetworkCard SetNetworkCard(NetworkCard networkCard, string key, string value)
        {
            switch (key)
            {
                case "MACAdress":
                    networkCard.MACAddress = value;
                    break;
                case "Description":
                    networkCard.Description = value;
                    break;
                case "GUID":
                    networkCard.GUID = value;
                    break;
                case "Netconnectionstatus":
                    networkCard.Netconnectionstatus = int.TryParse(value, out int ncs) ? ncs : -1;
                    break;
                case "Speed":
                    networkCard.Speed = int.TryParse(value, out int ncss) ? ncss : -1;
                    break;                              
                default:
                    break;
            }

            return networkCard;
        }

    }



}

