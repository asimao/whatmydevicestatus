﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent.Controllers
{
    class DiskDriveController
    {
        private static List<string> keysd = new List<string>() { "Name", "Caption", "Model", "InterfaceType", "Capabilities", "MediaLoaded", "MediaType", "Signature", "Status" };
        private static List<string> keysld = new List<string>() { "Name", "DeviceId", "Compressed", "DriveType", "FileSystem", "FreeSpace", "Size", "MediaType", "VolumeName", "VolumeSerialNumber" };

        public static List<DiskDrive> GetDiskDrives()
        {
            List<DiskDrive> diskDrives = new List<DiskDrive>();
            var driveQuery = new ManagementObjectSearcher("select * from Win32_DiskDrive");
            foreach (ManagementObject d in driveQuery.Get())
            {
                DiskDrive diskDrive = new DiskDrive();

                foreach (string s in keysd)
                {
                    if (WmiTool.CollectionWmiHaveValue(d, s))
                    {
                        SetDiskDrive(diskDrive, s, d[s].ToString(), "disk");
                    }
                }

                var deviceId = d.Properties["DeviceId"].Value;
                var partitionQueryText = string.Format("associators of {{{0}}} where AssocClass = Win32_DiskDriveToDiskPartition", d.Path.RelativePath);
                var partitionQuery = new ManagementObjectSearcher(partitionQueryText);
                foreach (ManagementObject p in partitionQuery.Get())
                {
                    var logicalDriveQueryText = string.Format("associators of {{{0}}} where AssocClass = Win32_LogicalDiskToPartition", p.Path.RelativePath);
                    var logicalDriveQuery = new ManagementObjectSearcher(logicalDriveQueryText);
                    foreach (ManagementObject ld in logicalDriveQuery.Get())
                    {
                        foreach (string s in keysld)
                        {
                            if (WmiTool.CollectionWmiHaveValue(ld, s))
                            {
                                SetDiskDrive(diskDrive, s, ld[s].ToString(), "logic");
                            }
                        }
                    }
                }

                diskDrives.Add(diskDrive);
            }

            return diskDrives;
        }

        public static DiskDrive SetDiskDrive(DiskDrive diskDrive, string key, string value, string type)
        {
            if(type == "disk")
            {
                switch (key)
                {
                    case "Name":
                        diskDrive.PhysicalName = value;
                        break;
                    case "Caption":
                        diskDrive.DiskName = value;
                        break;
                    case "Model":
                        diskDrive.DiskModel = value;
                        break;
                    case "InterfaceType":
                        diskDrive.DiskInterface = value;
                        break;
                    case "Capabilities":
                        diskDrive.Capabilities = value;
                        break;
                    case "MediaLoaded":
                        diskDrive.MediaLoaded = value;
                        break;
                    case "MediaType":
                        diskDrive.MediaType = value;
                        break;
                    case "Signature":
                        diskDrive.MediaSignature = value;
                        break;
                    case "Status":
                        diskDrive.MediaStatus = value;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (key)
                {
                    case "Name":
                        diskDrive.DriveName = value;
                        break;
                    case "DeviceId":
                        diskDrive.DriveId = value;
                        break;
                    case "Compressed":
                        diskDrive.DriveCompressed = value;
                        break;
                    case "DriveType":
                        diskDrive.DriveType = value;
                        break;
                    case "FileSystem":
                        diskDrive.FileSystem = value;
                        break;
                    case "FreeSpace":
                        diskDrive.FreeSpace = value;
                        break;
                    case "Size":
                        diskDrive.TotalSpace = value;
                        break;
                    case "MediaType":
                        diskDrive.DriveMediaType = value;
                        break;
                    case "VolumeName":
                        diskDrive.VolumeName = value;
                        break;
                    case "VolumeSerialNumber":
                        diskDrive.VolumeSerialNumber = value;
                        break;
                    default:
                        break;
                }
            }


            return diskDrive;
        }

    }
}
