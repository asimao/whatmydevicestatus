﻿using System.Collections.Generic;
using System.Management;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent.Controllers
{
    class SoftwareInstalledController
    {
        private static List<string> keys = new List<string>() { "Name", "Version", "InstallDate",/*"InstallLocation",*/"Vendor" };
        private static string table = "Win32_product";
        private static string path = @"\\localhost\ROOT\CIMV2";

        public static List<SoftwareInstalled> GetSoftwareInstalled()
        {
            List<ManagementObject> collection = WmiTool.GetWmiData(path, WmiTool.GetQuery(keys, table));
            List<SoftwareInstalled> softwareInstalleds = new List<SoftwareInstalled>();

            if (collection != null && collection.Count > 0)
            {
                foreach (ManagementObject oneObjectCollection in collection)
                {
                    SoftwareInstalled softwareInstalled = new SoftwareInstalled();
                    foreach (string s in keys)
                    {
                        if (WmiTool.CollectionWmiHaveValue(oneObjectCollection, s))
                        {
                            SetSoftwareInstalled(softwareInstalled, s, oneObjectCollection[s].ToString());
                        }
                    }

                    softwareInstalleds.Add(softwareInstalled);
                }
            }

            return softwareInstalleds;
        }

        public static SoftwareInstalled SetSoftwareInstalled(SoftwareInstalled softwareInstalled, string key, string value)
        {
            switch (key)
            {
                case "Name":
                    softwareInstalled.Name = value;
                    break;
                case "Version":
                    softwareInstalled.Version = value;
                    break;
                case "InstallDate":
                    softwareInstalled.InstallDate = value;
                    break;
                /*case "InstallLocation":
                    softwareInstalled.InstallLocation = value;
                    break;*/
                case "Vendor":
                    softwareInstalled.Vendor = value;
                    break;
                default:
                    break;
            }

            return softwareInstalled;
        }
    }
}


