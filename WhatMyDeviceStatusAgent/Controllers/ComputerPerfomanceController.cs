﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WhatMyDeviceStatusAgent.Entities;

namespace WhatMyDeviceStatusAgent.Controllers
{
    public class ComputerPerfomanceController
    {
        public static ComputerPerfomance GetComputerPerfomance()
        {
            return new ComputerPerfomance()
            {
                DiskDrivePerf = GetDiskDrivePerf(),
                CpuPerf = GetCpuPerf(),
                MemoryPerf = GetMemoryPerf()
            };
        }

        private static float GetDiskDrivePerf()
        {
            PerformanceCounter diskDriveCounter = new PerformanceCounter("PhysicalDisk", "% Disk Time", "_Total");
            CounterSample counterSample1 = diskDriveCounter.NextSample();
            Thread.Sleep(100);
            CounterSample counterSample2 = diskDriveCounter.NextSample();
            float finalDiskDriveCounter = CounterSample.Calculate(counterSample1, counterSample2);

            return finalDiskDriveCounter;
        }

        private static float GetMemoryPerf()
        {
            PerformanceCounter RamCounter = new PerformanceCounter("Memory", "% Committed Bytes In Use", null);

            return RamCounter.NextValue();
        }

        private static float GetCpuPerf()
        {
            PerformanceCounter CpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            CounterSample cs1 = CpuCounter.NextSample();
            Thread.Sleep(100);
            CounterSample cs2 = CpuCounter.NextSample();
            float finalCpuCounter = CounterSample.Calculate(cs1, cs2);

            return finalCpuCounter;
        }
    }
}
