﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent.Controllers
{
    class AntivirusController
    {
        private static List<string> keysAV = new List<string>() { "displayName", "productState" };
        private static string tableAV = "Antivirusproduct";
        private static string pathAV = @"\\localhost\ROOT\SecurityCenter2";


        public static List<Antivirus> GetAntivirusInstalled()
        {
            List<ManagementObject> collection = WmiTool.GetWmiData(pathAV, WmiTool.GetQuery(keysAV, tableAV));
            List<Antivirus> antivirusInstalleds = new List<Antivirus>();

            if (collection != null && collection.Count > 0)
            {
                foreach (ManagementObject oneObjectCollection in collection)
                {
                    Antivirus antivirusInstalled = new Antivirus();
                    foreach (string s in keysAV)
                    {
                        if (WmiTool.CollectionWmiHaveValue(oneObjectCollection, s))
                        {
                            SetAntivirusInstalled(antivirusInstalled, s, oneObjectCollection[s].ToString());
                        }
                    }
                    antivirusInstalleds.Add(antivirusInstalled);
                }
            }

            return antivirusInstalleds;
        }



        private static Antivirus SetAntivirusInstalled(Antivirus antivirusInstalled, string key, string value)
        {
            switch (key)
            {
                case "displayName":
                    antivirusInstalled.DisplayName = value;
                    break;
                case "productState":
                    antivirusInstalled=GetAVRealInformation(antivirusInstalled, value);
                    break;
                default:
                    break;
            }

            return antivirusInstalled;
        }

        private static Antivirus GetAVRealInformation(Antivirus antivirusInstalled, string value)
        {
            if (int.TryParse(value, out int intvalue))
            {
                List<string> ls = new List<string>();
                string FullAvInformation = intvalue.ToString("X").PadLeft(6, '0');
                string securityProvider = FullAvInformation.Substring(0, 2);
                string realTimeProtectionStatus = FullAvInformation.Substring(2, 2);
                string definitionStatus = FullAvInformation.Substring(4, 2);

                int securityProviderInt = int.Parse(FullAvInformation.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);

                List<char> securityProviderBin = new List<char> { '0' };
                securityProviderBin.AddRange(Convert.ToString(securityProviderInt, 2).PadLeft(7, '0')
                    .ToCharArray()
                    .Reverse()
                    .ToList());

                WhatMyDeviceStatusEntities wmdse = new WhatMyDeviceStatusEntities();
                List<AntivirusSecurityProvider> lasp = wmdse.AntivirusSecurityProvider.ToList();
                int y = -1;
                
                for (int i = 0; i < securityProviderBin.Count; i++)
                {
                    y = int.Parse(securityProviderBin[i].ToString()) != 0 ? i : -1;
                    if(y +1 > 0)
                    {
                        AntivirusSecurityProvider asp = lasp.Where(w => w.Id == y + 1).FirstOrDefault();
                        if(asp != null)
                        {
                            antivirusInstalled.AntivirusSecurityProviderRelational.Add(new AntivirusSecurityProviderRelational() { IdAntivirusSecurityProvider = asp.Id });
                        }
                    }
                    //switch (y)
                    //{
                    //    case -1:
                    //        break;
                    //    case 0:
                    //       ls.Add(AntivirusSecurityProvider.NONE.ToString());  //0
                    //        break;
                    //    case 1:
                    //       ls.Add(AntivirusSecurityProvider.FIREWALL.ToString()); //0
                    //        break;
                    //    case 2:
                    //       ls.Add(AntivirusSecurityProvider.AUTOUPDATE_SETTINGS.ToString()); //1
                    //        break;
                    //    case 3:
                    //       ls.Add(AntivirusSecurityProvider.ANTIVIRUS.ToString()); //1
                    //        break;
                    //    case 4:
                    //       ls.Add(AntivirusSecurityProvider.ANTISPYWARE.ToString()); //0
                    //        break;
                    //    case 5:
                    //       ls.Add(AntivirusSecurityProvider.INTERNET_SETTINGS.ToString()); //0
                    //        break;
                    //    case 6:
                    //       ls.Add(AntivirusSecurityProvider.USER_ACCOUNT_CONTROL.ToString()); //0
                    //        break;
                    //    case 7:
                    //       ls.Add(AntivirusSecurityProvider.SERVICE.ToString()); //0
                    //        break;
                    //    default:
                    //       ls.Add(AntivirusSecurityProvider.UNKNOWN.ToString()); //0
                    //        break;
                    //}
                }
            }

            //antivirusInstalled.SecurityProvider = JsonConvert.SerializeObject(ls);
            return antivirusInstalled;
        }
    }
}
