﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent.Controllers
{
    public class UsbDeviceController
    {
        private static List<string> keys = new List<string>() { "Status","Caption" };
        private static string table = "Win32_USBHub";
        private static string path = @"\\localhost\ROOT\CIMV2";

        public static List<UsbDevice> GetUsbDevices()
        {
            List<ManagementObject> collection = WmiTool.GetWmiData(path, WmiTool.GetQuery(keys, table));
            List<UsbDevice> usbDevices = new List<UsbDevice>();

            if (collection != null && collection.Count > 0)
            {
                foreach (ManagementObject oneObjectCollection in collection)
                {
                    UsbDevice usbDevice = new UsbDevice();
                    foreach (string s in keys)
                    {
                        if (WmiTool.CollectionWmiHaveValue(oneObjectCollection, s))
                        {
                            SetUsbDevice(usbDevice, s, oneObjectCollection[s].ToString());
                        }
                    }

                    usbDevices.Add(usbDevice);
                }
            }

            return usbDevices;
        }


        public static UsbDevice SetUsbDevice(UsbDevice usbDevice, string key, string value)
        {
            switch (key)
            {
                case "Status":
                    usbDevice.Status = value;
                    break;
                case "Caption":
                    usbDevice.Caption = value;
                    break;

                default:
                    break;
            }

            return usbDevice;
        }

    }
}
