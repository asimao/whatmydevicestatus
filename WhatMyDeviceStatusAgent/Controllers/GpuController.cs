﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent.Controllers
{
    class GpuController
    {
        private static List<string> keys = new List<string>() { "Description", "DriverDate" };
        private static string table = "Win32_VideoController";
        private static string path = @"\\localhost\ROOT\CIMV2";

        public static List<Gpu> GetGpus()
        {
            List<ManagementObject> collection = WmiTool.GetWmiData(path, WmiTool.GetQuery(keys, table));
            List<Gpu> gpus = new List<Gpu>();

            if (collection != null && collection.Count > 0)
            {
                foreach (ManagementObject oneObjectCollection in collection)
                {
                    Gpu gpu = new Gpu();
                    foreach (string s in keys)
                    {
                        if (WmiTool.CollectionWmiHaveValue(oneObjectCollection, s))
                        {
                            SetGpu(gpu, s, oneObjectCollection[s].ToString());
                        }
                    }

                    gpus.Add(gpu);
                }
            }

            return gpus;
        }

        public static Gpu SetGpu(Gpu gpu, string key, string value)
        {
            switch (key)
            {
                case "Description":
                    gpu.Description = value;
                    break;
                case "DriverDate":
                    gpu.DriverDate = value;
                    break;

                default:
                    break;
            }

            return gpu;
        }

    }
}
