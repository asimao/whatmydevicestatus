﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace WhatMyDeviceStatusAgent.Tools
{
    public class WmiTool
    {
        public static string GetQuery(List<string> keys, string table)
        {
            string request = "SELECT ";

            foreach (string s in keys)
            {
                request += s + ",";
            }

            request = request.Substring(0, request.Length - 1);
            request += " FROM " + table;

            return request;
        }

        public static List<ManagementObject> GetWmiData(string path, string query)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(path, query);
            ManagementObjectCollection objectCollection = searcher.Get();
            List<ManagementObject> collection = new List<ManagementObject>();
            try
            {
                collection = objectCollection.Cast<ManagementObject>().ToList();
            }
            catch
            {
                collection = null;
            }

            return collection;
        }

        public static bool CollectionWmiHaveValue(ManagementObject oneObjectCollection, string index)
        {
            try
            {
                return (oneObjectCollection[index] != null &&
                                !String.IsNullOrEmpty(oneObjectCollection[index].ToString().Trim()));
            }
            catch
            {
                return false;
            }
        }
    }
}
