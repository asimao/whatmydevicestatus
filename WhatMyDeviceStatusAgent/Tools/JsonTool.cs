﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WhatMyDeviceStatusAgent.Tools
{
    public class JsonTool
    {
        public static JsonSerializerSettings JsonSetting()
        {
            JsonSerializerSettings jss = new JsonSerializerSettings
            {
                Formatting = Formatting.None,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                TypeNameHandling = TypeNameHandling.None,
            };

            jss.Converters.Add(new StringEnumConverter());

            return jss;
        }
    }
}
