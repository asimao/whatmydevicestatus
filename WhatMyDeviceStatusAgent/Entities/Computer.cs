//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WhatMyDeviceStatusAgent.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Computer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Computer()
        {
            this.Alerts = new HashSet<Alerts>();
            this.Antivirus = new HashSet<Antivirus>();
            this.ComputerPerfomance = new HashSet<ComputerPerfomance>();
            this.Cpu = new HashSet<Cpu>();
            this.CurrentOperatingSystem = new HashSet<CurrentOperatingSystem>();
            this.DiskDrive = new HashSet<DiskDrive>();
            this.Gpu = new HashSet<Gpu>();
            this.NetworkCard = new HashSet<NetworkCard>();
            this.Ram = new HashSet<Ram>();
            this.SoftwareInstalled = new HashSet<SoftwareInstalled>();
            this.UsbDevice = new HashSet<UsbDevice>();
        }
    
        public int Id { get; set; }
        public string IdentifyingNumber { get; set; }
        public string UUID { get; set; }
        public string Name { get; set; }
        public string Vendor { get; set; }
        public System.DateTime AddDate { get; set; }
        public string LastUpdateError { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public Nullable<System.DateTime> DisabledDate { get; set; }
        public bool Disabled { get; set; }
        public Nullable<int> IdUser { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Alerts> Alerts { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Antivirus> Antivirus { get; set; }
        public virtual Users Users { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ComputerPerfomance> ComputerPerfomance { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cpu> Cpu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CurrentOperatingSystem> CurrentOperatingSystem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DiskDrive> DiskDrive { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Gpu> Gpu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NetworkCard> NetworkCard { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ram> Ram { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SoftwareInstalled> SoftwareInstalled { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsbDevice> UsbDevice { get; set; }
    }
}
