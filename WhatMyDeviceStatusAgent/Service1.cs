﻿using Newtonsoft.Json;
using RestSharp;
using System.ServiceProcess;
using System.Timers;
using WhatMyDeviceStatusAgent.Controllers;
using WhatMyDeviceStatusAgent.Entities;
using WhatMyDeviceStatusAgent.Tools;

namespace WhatMyDeviceStatusAgent
{
    public partial class Wmds : ServiceBase
    {
        private int seconds = 0;
        private Timer aTimer;
        private static string mainUrl = "http://localhost/";

        public Wmds()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            InitTimer();
        }

        private void InitTimer()
        {
            aTimer = new Timer(10000);
            aTimer.Elapsed += TimerElapsed;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
            aTimer.Start();
        }

        private void DestroyTimer()
        {
            aTimer.Stop();
            aTimer.Dispose();
            aTimer = null;
        }
        // ComputerPerfomance toutes les 60 secondes (1 minute) et ComputerPerformance toutes les 4 heures 
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            DestroyTimer();

            if (seconds == 60 || seconds >= 14460) 
            {
                SendComputerRequest(ComputerController.GetComputer());

                if(seconds >= 14460)
                    seconds = 0;
            }
            else
            {
                SendPerformanceRequest(ComputerController.GetComputerInformation().UUID,
                    ComputerPerfomanceController.GetComputerPerfomance());
            }

            seconds += 10;

            InitTimer();
        }


        private void SendComputerRequest(Computer computer)
        {
            try
            {
                string jsonComputer = JsonConvert.SerializeObject(computer, JsonTool.JsonSetting());

                RestClient client = new RestClient(mainUrl + "Computer/SetComputerData");
                RestRequest request = new RestRequest()
                {
                    Method = Method.POST,
                };
                request.AddParameter("computer", jsonComputer, ParameterType.GetOrPost);
                IRestResponse restResponse = client.Execute(request);
            }
            catch{}
        }


        private void SendPerformanceRequest(string uuid, ComputerPerfomance computerPerfomance)
        {
            try
            {
                string jsonComputerPerfomance = JsonConvert.SerializeObject(computerPerfomance, JsonTool.JsonSetting());

                RestClient client = new RestClient(mainUrl + "Computer/SetComputerPerfomanceData");
                RestRequest request = new RestRequest()
                {
                    Method = Method.POST,
                };
                request.AddParameter("uuid", uuid, ParameterType.GetOrPost);
                request.AddParameter("computerPerfomance", jsonComputerPerfomance, ParameterType.GetOrPost);
                IRestResponse restResponse = client.Execute(request);
            }
            catch { };
        }

        protected override void OnStop()
        {
            aTimer.Stop();
        }
    }
}
